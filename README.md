# Unifydmin

Unified administration panel for heterogeneous Linux servers.

# Inspiration

There are quite a few ways to control multiple servers at once with one unified tool, but none of them works as good as it should, for different reasons.

## Cockpit

Cockpit is a web interface to manage servers. To work, it needs to be installed on the server, and this is annoying and unnecessary. Also, it requires the distribution to actually have this package in their repos, and poses the usual issues regarding version mismatches.

In short:

- Needs installed component
  - Distros need to package it
  - Different distros => different versions in production => issues everywhere
- Can add "slave" servers to a "master" server (*do they still need the cockpit package?*)
  - How do you define a "master" server? Are you supposed to install it on your workstation? Or on a gateway? Unclear
  - More likely meant to control just the "master" server
- Limited functionality

## Ansible

Ansible is a command line automation tool to manage different Linux (\*Nix?) machines by defining "playbooks", basically over-complicated scripts that should enable the user to perform logic actions all at once on a specific set of servers.

In short:

- Needs **NO** server side software (except ssh and python), just client side
- Playbooks (yaml) are over complicated to the point that scripting a system update for n servers is a non-trivial task by itself
- One button ssh connection (*integrated terminal?*)
- Many plugins, they try to use the same "scheme", but fail 
- Config file is over complicated and difficult to understand by itself

# The idea

- No server side component (just like Ansible)
- Performance monitoring and logging (like Cockpit)
- "App store" like approach (like Docker) **?**
- Define system class (by distro, stack...) and generalize commands like *update* or *restart httpd* accordingly
- Monitoring of parameters like open ports or running/enabled services (*systemd only?*)
- Scan system for known vulnerabilities based on package versions **?**
- Manage libvirtd vms?
- Manage docker images?
- Centralized monitoring

# Implementation overview

- (Backend) Language: python
- GUI: *undefined*
  - Candidates:
    - ~~Web: Electron or local server. Pros: portable, easy to run everywhere. Cons: heavy, possibly clunky, is out of place graphically speaking~~
    - **GTK**: Native and sleek. Pros: fast, native, beautiful. Cons: not very portable, plots could be annoying to make
    - ~~Qt: Native and somewhat decent. Pros: fast, decently portable. Cons: mostly C++, ugly-ish everywhere~~
  - Winner: **GTK**
- Communication protocol
  - SSH
  - Data is mostly dictionaries, can be something else
  - ~~Logins encrypted with master password?~~ Logins are managed with rsa keys
- Systems
  - TODO
- Actions
  - Every action follows the same structure
  - As simple as possible for the remote host, most of the processing done client side
  - Receive actions keep the result of the last execution
  - *(Maybe)* Actions *could* inject python programs onto the target server if there is a need for more specific actions. Again, maybe.
- WatcherActions
  - TODO

# Backend strucutre

*UML here?*

# Building

## Using GNOME Builder

- Install both GNOME Builder and flatpak on your system
- Use the clone option to clone this repository
- Build and Run using the GUI. It should automatically find the flatpak manifest, build all the dependencies and run the application

## Using flatpak-builder

This section will walk you through creating a flatpak bundle and installing it on your system.

```bash
git clone https://gitlab.com/gabmus/unifydmin
mkdir unifydmin_flatpak
cd unifydmin_flatpak
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak-builder --repo=repo unifydmin ../unifydmin/dist/flatpak/org.gabmus.unifydmin.json --force-clean
flatpak build-bundle repo unifydmin-$(git -C ../unifydmin describe --long --tags --always).flatpak org.gabmus.unifydmin
flatpak --user install unifydmin-$(git -C ../unifydmin describe --long --tags --always).flatpak
```

## Using meson

First you need to install all of the dependencies on your system. Depending on your distribution this may be more or less difficult. The dependencies are listed in this project repository in `misc/dependencies.txt`.

Once the dependencies are installed, follow these steps.

```bash
git clone https://gitlab.com/gabmus/unifydmin
cd unifydmin
mkdir build
cd build
meson ..
meson configure -Dprefix=$(pwd)/testdir
ninja
ninja install && ninja run
```

To run the application again, just cd into the build directory and run `ninja install && ninja run`.

This *will not* install anything in your system directories, but instead it will install into the build/testdir directory.
