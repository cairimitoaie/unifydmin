from gi.repository import Gtk, Gio, Gdk, Handy

from .appWindow import UnifydminAppWindow
from .settingsWindow import UnifydminSettingsWindow
from .confManager import ConfManager

import os, sys


class Application(Gtk.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.conf_manager = ConfManager()

        self.window = UnifydminAppWindow()
        self.window.set_icon_name('org.gabmus.unifydmin')
        self.window.set_title('Unifydmin')

        self.window.connect('destroy', self.on_destroy_window)
        # self.leaflet.connect('notify::folded', self.show_hide_back)

    # def change_fold(self, btn):
    #    self.leaflet.switch_visible()

    def do_startup(self):
        Gtk.Application.do_startup(self)  # no, super().do_startup() doesn't work

        actions = [
            {
                'name': 'settings',
                'func': self.show_settings_window
            },
            {
                'name': 'shortcuts',
                'func': self.show_shortcuts_window
            },
            {
                'name': 'about',
                'func': self.show_about_dialog
            },
            {
                'name': 'quit',
                'func': self.on_destroy_window
            }
        ]

        for a in actions:
            c_action = Gio.SimpleAction.new(a['name'], None)
            c_action.connect('activate', a['func'])
            self.add_action(c_action)

    def show_about_dialog(self, *args):
        builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/aboutdialog.glade'
        )
        dialog = builder.get_object('aboutdialog')
        dialog.set_transient_for(self.window)
        dialog.set_modal(True)
        dialog.present()

    def show_settings_window(self, *args):
        settings_win = UnifydminSettingsWindow()
        settings_win.set_transient_for(self.window)
        settings_win.set_attached_to(self.window)
        settings_win.set_modal(True)
        settings_win.present()

    def show_shortcuts_window(self, *args):
        shortcuts_win = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/shortcutsWindow.glade'
        ).get_object('shortcuts-unifydmin')
        shortcuts_win.props.section_name = 'shortcuts'
        shortcuts_win.set_transient_for(self.window)
        shortcuts_win.set_attached_to(self.window)
        shortcuts_win.set_modal(True)
        shortcuts_win.present()
        shortcuts_win.show_all()

    def do_activate(self):
        self.add_window(self.window)
        stylecontext = Gtk.StyleContext()
        provider = Gtk.CssProvider()
        provider.load_from_data('''
            .rounded_frame {
                border-radius: 6px;
            }
        '''.encode())
        stylecontext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.window.present()
        self.window.show_all()
        # self.show_hide_back()

    def on_destroy_window(self, *args):
        self.window.hide()
        self.conf_manager.stop_threads = True
        self.conf_manager.watcher_runner.stop_thread()
        while Gtk.events_pending():
            Gtk.main_iteration()
        self.quit()


def main():
    application = Application()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
