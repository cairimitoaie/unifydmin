from gi.repository import Gtk

class StackChild(Gtk.Bin):
    def __init__(self, name: str, child_widget, **kwargs):
        super().__init__(**kwargs)
        self.__name = name
        self.child_widget = child_widget
 
        self.add(self.child_widget)

    @property
    def name(self):
        return self.__name


class UnifydminAppStack(Gtk.Stack):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.set_hexpand(True)

        self.set_transition_type(Gtk.StackTransitionType.CROSSFADE)

    def add_and_get_stackchild(self, name: str, widget):
        n_child = StackChild(name, widget)
        self.add_named(n_child, name)
        return n_child
        
