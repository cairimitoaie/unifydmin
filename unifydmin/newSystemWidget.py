from gi.repository import Gtk
from .confManager import ConfManager


class UnifydminNewSystemWidget(Gtk.Bin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/newSystemWidget.glade'
        )
        self.add(self.builder.get_object('newSystemBox'))

        self.confman = ConfManager()

        self.entry_name = self.builder.get_object('entryName')
        self.entry_username = self.builder.get_object('entryUsername')
        self.entry_address = self.builder.get_object('entryAddress')
        self.entry_port = self.builder.get_object('entryPort')
        self.connect_button = self.builder.get_object('connectButton')
        self.error_label = self.builder.get_object('errorLabel')
        self.error_label.set_no_show_all(True)

        self.builder.connect_signals(self)

    def clear_fields(self):
        self.entry_name.set_text('')
        self.entry_username.set_text('')
        self.entry_address.set_text('')

    def __fields_filled(self):
        return not not (
                self.entry_name.get_text() and
                self.entry_username.get_text() and
                self.entry_address.get_text() and
                self.entry_port.get_text()
        )

    def __update_connect_button_sensitive(self):
        self.connect_button.set_sensitive(
            self.__fields_filled() and not self.confman.exists_system_with_name(
                self.entry_name.get_text()
            )
        )

    def on_entryName_changed(self, entry):
        self.__update_connect_button_sensitive()

    def on_entryUsername_changed(self, entry):
        self.__update_connect_button_sensitive()

    def on_entryAddress_changed(self, entry):
        self.__update_connect_button_sensitive()
