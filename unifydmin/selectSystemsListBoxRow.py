from gi.repository import Gtk, Pango, GtkSource
from .initialsIcon import UnifydminInitialsIcon
from .core.broker import Broker

class SpinnerOrCheck(Gtk.Box):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.spinner = Gtk.Spinner()
        self.spinner.start()
        self.check = Gtk.Image.new_from_icon_name('emblem-ok-symbolic', Gtk.IconSize.BUTTON)

        self.add(self.spinner)
        self.add(self.check)
        self.spinner.set_no_show_all(True)
        self.check.set_no_show_all(True)
        self.show_none()

    def show_spinner(self):
        self.spinner.show()
        self.check.hide()

    def show_check(self):
        self.spinner.hide()
        self.check.show()

    def show_none(self):
        self.spinner.hide()
        self.check.hide()


class OutputRevealer(Gtk.Revealer): # access using set_reveal_child(bool)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.source_buffer = GtkSource.Buffer()
        self.source_buffer.set_text('')
        #self.source_buffer.set_style_scheme # TODO: complete, make dark
        self.scrolled_win = Gtk.ScrolledWindow()

        self.source_view = GtkSource.View.new_with_buffer(self.source_buffer)
        self.source_view.set_editable(False)
        self.source_view.set_monospace(True)

        self.set_transition_type(Gtk.RevealerTransitionType.SLIDE_DOWN)

        self.scrolled_win.add(self.source_view)
        self.scrolled_win.set_policy(
            Gtk.PolicyType.AUTOMATIC,
            Gtk.PolicyType.NEVER
        )
        self.add(self.scrolled_win)

    def set_data(self, data: str):
        self.source_buffer.set_text(data)


class OutputButton(Gtk.ToggleButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.container_box = Gtk.Box(Gtk.Orientation.HORIZONTAL)
        self.label = Gtk.Label('Output')
        self.icon = Gtk.Image.new_from_icon_name('pan-down-symbolic', Gtk.IconSize.BUTTON)
        self.container_box.add(self.label)
        self.container_box.add(self.icon)
        self.add(self.container_box)
        self.set_valign(Gtk.Align.CENTER)
        self.set_no_show_all(True)
        self.hide()

    def update_icon(self):
        self.icon.set_from_icon_name(
            'pan-up-symbolic' if self.get_active() else 'pan-down-symbolic',
            Gtk.IconSize.BUTTON
        )

    def show_inner(self):
        self.container_box.show_all()


class UnifydminSelectSystemsListBoxRow(Gtk.ListBoxRow):
    def __init__(self, system, action, **kwargs):
        super().__init__(**kwargs)
        self.system = system
        self.action = action
        self.broker = Broker(self.system, self.action)

        self.name = self.system.name
        self.selected = False
        self.running = False
        self.__has_output = False

        self.initials_icon = UnifydminInitialsIcon(self.name)
        self.name_label = Gtk.Label(self.name)
        self.name_label.set_hexpand(False)
        self.name_label.set_halign(Gtk.Align.START)
        self.name_label.set_ellipsize(Pango.EllipsizeMode.END)
        self.checkbox = Gtk.CheckButton()
        self.checkbox.set_active(self.selected)
        self.checkbox.set_valign(Gtk.Align.CENTER)
        self.checkbox.set_halign(Gtk.Align.CENTER)
        self.checkbox.connect('toggled', self.on_checkbox_toggled)
        self.spinner_or_check = SpinnerOrCheck()

        self.output_button = OutputButton()
        self.output_revealer = OutputRevealer()

        self.super_container = Gtk.Box(Gtk.Orientation.VERTICAL)
        self.super_container.set_orientation(Gtk.Orientation.VERTICAL)
        self.super_container.set_homogeneous(False)

        self.container_box = Gtk.Box(Gtk.Orientation.HORIZONTAL)
        self.container_box.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.container_box.pack_start(self.initials_icon, False, False, 6)
        self.container_box.pack_start(self.name_label, True, True, 6)
        self.container_box.pack_end(self.output_button, False, False, 6)
        self.container_box.pack_end(self.spinner_or_check, False, False, 6)
        self.container_box.pack_end(self.checkbox, False, False, 0)
        
        self.super_container.add(self.container_box)
        self.super_container.add(self.output_revealer)
        self.add(self.super_container)

        self.output_button.connect('toggled', self.on_output_button_toggled)

    def run_action(self):
        self.start_running()
        self.broker.run()

    def on_output_button_toggled(self, *args):
        self.output_button.update_icon()
        self.output_revealer.set_reveal_child(
            self.output_button.get_active()
        )

    def set_output(self, data: str):
        self.output_revealer.set_data(data)

    def on_checkbox_toggled(self, checkbox):
        self.selected = checkbox.get_active()

    def toggle_selected(self):
        self.selected = not self.selected
        self.checkbox.set_active(self.selected)

    def start_running(self):
        self.reset_running()
        self.set_sensitive(False)
        self.checkbox.set_sensitive(False)
        self.spinner_or_check.show_spinner()
        self.running = True

    def done_running(self):
        self.set_sensitive(True)
        self.checkbox.set_sensitive(True)
        # NOTE: self.check is still insensitive until reset. REVISED, maybe not?
        self.output_button.show()
        self.output_button.show_all()
        self.output_button.show_inner()
        self.spinner_or_check.show_check()
        self.running = False

    def reset_running(self):
        self.set_sensitive(True)
        self.checkbox.set_sensitive(True)
        self.checkbox.set_active(False)
        self.selected = False
        self.running = False
        self.spinner_or_check.show_none()
        self.output_button.hide()
