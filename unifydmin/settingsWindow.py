from gi.repository import Gtk, Gdk
from .confManager import ConfManager


class UnifydminSettingsWindow(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.confman = ConfManager()

        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_type_hint(Gdk.WindowTypeHint.DIALOG)

        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/settingsWindow.glade'
        )

        self.set_titlebar(self.builder.get_object('settingsHeaderbar'))
        self.add(self.builder.get_object('settingsBox'))

        self.terminal_cmd_entry = self.builder.get_object('terminalCmdEntry')
        self.terminal_cmd_entry.set_text(self.confman.conf['terminal_cmd'])

        self.rsa_key_filepath_entry = self.builder.get_object('rsaKeyFilepathEntry')
        self.rsa_key_filepath_entry.set_text(self.confman.conf['rsa_key_filepath'])

        self.rsa_key_passphrase_entry = self.builder.get_object('rsaKeyPassphraseEntry')
        self.rsa_key_passphrase_entry.set_visibility(False)

        if self.confman.conf.get('rsa_key_passphrase'):
            self.rsa_key_passphrase_entry.set_text(self.confman.conf['rsa_key_passphrase'])

        if self.confman.conf.get('rsa_key_filepath'):
            self.rsa_key_filepath_entry.set_text(self.confman.conf['rsa_key_filepath'])

        self.builder.connect_signals(self)

    def on_rsaKeyFilepath_focus_in_event(self, widget, event):

        chooser = Gtk.FileChooserDialog(title="Open File", action=Gtk.FileChooserAction.OPEN, buttons=(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = chooser.run()
        if response == Gtk.ResponseType.OK:
            # The user has selected a file
            self.rsa_key_filepath_entry.set_text(chooser.get_filename())

        chooser.destroy()

        assert isinstance(self.terminal_cmd_entry, Gtk.Entry)
        self.terminal_cmd_entry.grab_focus()

    def on_terminalCmdSaveButton_clicked(self, btn):
        self.confman.conf['terminal_cmd'] = self.terminal_cmd_entry.get_text()
        self.confman.conf['rsa_key_filepath'] = self.rsa_key_filepath_entry.get_text()
        self.confman.conf['rsa_key_passphrase'] = self.rsa_key_passphrase_entry.get_text()

        # rebuild systems
        self.confman.save_conf()

        for system in self.confman.systems:
            system.reload(self.confman.conf.get('rsa_key_filepath'), self.confman.conf.get('rsa_key_passphrase'))

    def on_terminalCmdResetDefault_clicked(self, btn):
        self.terminal_cmd_entry.set_text(self.confman.BASE_SCHEMA.get('terminal_cmd'))
        self.rsa_key_passphrase_entry.set_text(self.confman.BASE_SCHEMA.get('rsa_key_passphrase'))
        self.rsa_key_filepath_entry.set_text(self.confman.BASE_SCHEMA.get('rsa_key_filepath'))
