from gi.repository import Gtk, GObject
from .sidebarListBoxRow import UnifydminSidebarSystemListBoxRow, UnifydminSidebarActionListBoxRow
from .newSystemWidget import UnifydminNewSystemWidget

class LeftHeaderbar(Gtk.Bin):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.menu_builder = Gtk.Builder.new_from_string('''
            <?xml version="1.0" encoding="UTF-8"?>
            <interface>
                <menu id="generalMenu">
                    <section>
                        <item>
                            <attribute name="label">Settings</attribute>
                            <attribute name="action">app.settings</attribute>
                        </item>
                        <item>
                            <attribute name="label">Shortcuts</attribute>
                            <attribute name="action">app.shortcuts</attribute>
                        </item>
                        <item>
                            <attribute name="label">About</attribute>
                            <attribute name="action">app.about</attribute>
                        </item>
                        <item>
                            <attribute name="label">Quit</attribute>
                            <attribute name="action">app.quit</attribute>
                        </item>
                    </section>
                </menu>
            </interface>
        ''', -1)

        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/leftHeaderbar.glade'
        )
        
        self.headerbar = self.builder.get_object('leftHeaderbar')
        self.headerbar.set_show_close_button(True)
        self.new_system_popover = self.builder.get_object('newSystemPopover')
        self.new_system_widget = UnifydminNewSystemWidget()
        self.new_system_popover.add(self.new_system_widget)

        self.general_menu = self.menu_builder.get_object('generalMenu')
        self.general_menu_popover = self.builder.get_object('generalMenuPopover')
        self.general_menu_popover.bind_model(self.general_menu)

        self.add(self.headerbar)
    
        self.builder.connect_signals(self)
    
    def on_newSystemBtn_clicked(self, btn):
        self.new_system_popover.popup()
        self.new_system_widget.show_all()

    def on_generalMenuButton_clicked(self, btn):
        self.general_menu_popover.popup()


class UnifydminSidebar(Gtk.Bin):

    __gsignals__ = {'unifydmin_stack_changed': (
        GObject.SIGNAL_RUN_FIRST,
        None,
        (Gtk.Widget,)
    )}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/sidebar.glade'
        )
        self.add(self.builder.get_object('sidebarBox'))
        
        self.systems_revealer = self.builder.get_object('systemsRevealer')
        self.actions_revealer = self.builder.get_object('actionsRevealer')

        self.systems_arrow = self.builder.get_object('systemsRevealerArrow')
        self.actions_arrow = self.builder.get_object('actionsRevealerArrow')

        # state of the revealer, True is open, False is closed
        self.systems_revealer_open = True
        self.actions_revealer_open = True
        self.systems_revealer.set_reveal_child(self.systems_revealer_open)
        self.actions_revealer.set_reveal_child(self.actions_revealer_open)
        self.change_arrow(self.systems_arrow, self.systems_revealer_open)
        self.change_arrow(self.actions_arrow, self.actions_revealer_open)

        self.systems_listbox = self.builder.get_object('systemsListbox')
        self.actions_listbox = self.builder.get_object('actionsListbox')

        self.headerbar = LeftHeaderbar()

        self.main_stack = None
        self.headerbar_stack = None
    
        self.builder.connect_signals(self)

    def set_stacks(self, main_stack, headerbar_stack):
        self.main_stack = main_stack
        self.headerbar_stack = headerbar_stack

    def on_systemsRevealerTrigger_button_press_event(self, *args):
        self.systems_revealer_open = not self.systems_revealer_open
        self.systems_revealer.set_reveal_child(self.systems_revealer_open)
        self.change_arrow(self.systems_arrow, self.systems_revealer_open)

    def on_actionsRevealerTrigger_button_press_event(self, *args):
        self.actions_revealer_open = not self.actions_revealer_open
        self.actions_revealer.set_reveal_child(self.actions_revealer_open)
        self.change_arrow(self.actions_arrow, self.actions_revealer_open)

    def change_arrow(self, arrow, state):
        arrow.set_from_icon_name(
            'pan-down-symbolic' if state else 'pan-end-symbolic',
            Gtk.IconSize.MENU
        )

    def __populate_generic_list(self, items: list, listbox: Gtk.ListBox, RowCls):
        # empty listbox first
        while True:
            row = listbox.get_row_at_index(0)
            if row:
                listbox.remove(row)
            else:
                break
        # now fill it up
        for i in items:
            listbox.add(RowCls(i))
        listbox.show_all()

    def populate_systems_list(self, systems: list):
        self.__populate_generic_list(
            systems,
            self.systems_listbox,
            UnifydminSidebarSystemListBoxRow
        )

    def populate_actions_list(self, actions: list):
        self.__populate_generic_list(
            actions,
            self.actions_listbox,
            UnifydminSidebarActionListBoxRow
        )

    def emit_change_signal(self):
        self.emit('unifydmin_stack_changed', self)

    def on_systemsListbox_row_activated(self, listbox, row):
        if not self.main_stack:
            return
        self.main_stack.set_visible_child_name(row.name)
        self.headerbar_stack.set_visible_child_name(row.name)
        self.actions_listbox.select_row(None)
        self.emit_change_signal()

    def on_actionsListbox_row_activated(self, listbox, row):
        if not self.main_stack:
            return
        self.main_stack.set_visible_child_name(row.name)
        self.headerbar_stack.set_visible_child_name(row.name)
        self.systems_listbox.select_row(None)
        self.emit_change_signal()

    def select_next_row(self, *args):
        a_sel_row = self.actions_listbox.get_selected_row()
        s_sel_row = self.systems_listbox.get_selected_row()
        if not s_sel_row and not a_sel_row:
            target = self.systems_listbox.get_children()[0]
            self.systems_listbox.select_row(target)
            self.on_systemsListbox_row_activated(self.systems_listbox, target)
            return
        elif not a_sel_row:
            # a system is selected
            rows = self.systems_listbox.get_children()
            if s_sel_row == rows[-1]:
                # last system selected? select first action
                target = self.actions_listbox.get_children()[0]
                self.actions_listbox.select_row(target)
                self.on_actionsListbox_row_activated(self.actions_listbox, target)
                return
            else:
                # get next system
                for i, row in enumerate(rows):
                    # can't be the last system, this case is the previous if
                    if row == s_sel_row:
                        target = rows[i+1]
                        self.systems_listbox.select_row(target)
                        self.on_systemsListbox_row_activated(self.systems_listbox, target)
                        return
        elif not s_sel_row:
            rows = self.actions_listbox.get_children()
            if a_sel_row == rows[-1]:
                # last action selected? do nothing
                return
            else:
                # get next action
                for i, row in enumerate(rows):
                    # can't be the last action, this case is the previous if
                    if row == a_sel_row:
                        target = rows[i+1]
                        self.actions_listbox.select_row(target)
                        self.on_actionsListbox_row_activated(self.actions_listbox, target)
                        return


    def select_prev_row(self, *args):
        a_sel_row = self.actions_listbox.get_selected_row()
        s_sel_row = self.systems_listbox.get_selected_row()
        if not s_sel_row and not a_sel_row:
            target = self.systems_listbox.get_children()[0]
            self.systems_listbox.select_row(target)
            self.on_systemsListbox_row_activated(self.systems_listbox, target)
            return
        elif not a_sel_row:
            # a system is selected
            rows = self.systems_listbox.get_children()
            if s_sel_row == rows[0]:
                # first system selected? do nothing
                return
            else:
                # get prev system
                for i, row in enumerate(rows):
                    # can't be the first system, this case is the previous if
                    if row == s_sel_row:
                        target = rows[i-1]
                        self.systems_listbox.select_row(target)
                        self.on_systemsListbox_row_activated(self.systems_listbox, target)
                        return
        elif not s_sel_row:
            rows = self.actions_listbox.get_children()
            if a_sel_row == rows[0]:
                # first action selected? select last system
                target = self.systems_listbox.get_children()[-1]
                self.systems_listbox.select_row(target)
                self.on_systemsListbox_row_activated(self.systems_listbox, target)
                return
            else:
                # get prev action
                for i, row in enumerate(rows):
                    # can't be the first action, this case is the previous if
                    if row == a_sel_row:
                        target = rows[i-1]
                        self.actions_listbox.select_row(target)
                        self.on_actionsListbox_row_activated(self.actions_listbox, target)
                        return
