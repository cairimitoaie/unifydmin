import threading
from gi.repository import Gtk
from time import sleep

def __sleep(time):
    sleep(time)

def async_sleep(time):
    t = threading.Thread(
        group = None,
        target = __sleep,
        name = None,
        args = (time,)
    )
    t.start()
    while t.is_alive():
        while Gtk.events_pending():
            Gtk.main_iteration()
    return
