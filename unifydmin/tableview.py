from gi.repository import Gtk, GObject, GLib
from .confManager import ConfManager
from .core.watcher_runner import WatcherRunner

class RevealerWithArrow(Gtk.Bin):
    def __init__(self, label_text: str, **kwargs):
        super().__init__(**kwargs)

        self.showing = True
        self.builder = Gtk.Builder.new_from_resource(
            '/org/gabmus/unifydmin/ui/revealerWithArrow.glade'
        )

        self.revealer = self.builder.get_object('revealer')
        self.label = self.builder.get_object('label')
        self.label.set_text(label_text)
        self.arrow_icon = self.builder.get_object('arrowIcon')

        self.add(self.builder.get_object('revealerWithArrowBox'))
        self.builder.connect_signals(self)

    def add_to_revealer(self, widget):
        self.revealer.add(widget)

    def on_eventBox_button_press_event(self, *args):
        self.showing = not self.showing
        self.revealer.set_reveal_child(self.showing)
        self.arrow_icon.set_from_icon_name(
            'pan-down-symbolic' if self.showing else 'pan-end-symbolic',
            Gtk.IconSize.MENU
        )

class UnifydminTableView(Gtk.Bin):
    def __init__(self, watcher, **kwargs):
        super().__init__(**kwargs)
        self.watcher = watcher
        self.watcher_runner = WatcherRunner()
        self.watcher_runner.connect('unifydmin_watcher_new_data', self.on_watcher_new_data)
        self.confman = ConfManager()
        self.prev_data = None

        self.scrolled_win = Gtk.ScrolledWindow()
        self.revealer_with_arrow = RevealerWithArrow(self.watcher.name)

        self.columns = self.watcher.columns
        # *tuple expands a tuple
        # ListStore() wants the type of each column as an argument
        # this makes it possible to create liststores of programmatic size
        self.store = Gtk.ListStore(*(str,)*len(self.columns))
        self.treeview = Gtk.TreeView(self.store)
        #self.renderer = Gtk.CellRendererText()
        #self.tree_columns = []
        for i, val in enumerate(self.columns):
            renderer = Gtk.CellRendererText()
            tc = Gtk.TreeViewColumn(val, renderer, text=i)
            #self.tree_columns.append(tc)
            self.treeview.append_column(tc)

        self.scrolled_win.add(self.treeview)
        self.scrolled_win.set_policy(
            Gtk.PolicyType.AUTOMATIC,
            Gtk.PolicyType.NEVER
        )
        self.set_no_show_all(True)
        self.revealer_with_arrow.add_to_revealer(self.scrolled_win)
        self.add(self.revealer_with_arrow)

    def on_watcher_new_data(self, *args):
        data = self.watcher.data[0]
        if type(data) == list and data != self.prev_data:
            GLib.idle_add(
                self.refresh_data_view,
                data
            )
            self.prev_data = data

    def refresh_data_view(self, data: list):
        self.store.clear()
        for row in data:
            n_data = []
            for key in self.columns:
                n_data.append(row[key])
            self.store.append(n_data)
        self.show()
        self.revealer_with_arrow.show_all()
